import React from "react";
import PropTypes from "prop-types";
function Footer({ title = "Footer title" }) {
  return <h3 style={{ fontSize: "20px" }}>{title}</h3>;
}
Footer.propTypes = {
  title: PropTypes.string,
};
export default Footer;
