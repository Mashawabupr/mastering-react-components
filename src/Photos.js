import React from "react";
import PropTypes from "prop-types";
import "./Photos.css";
function Photos({ photosArray }) {
  return (
    <ul>
      {photosArray.slice(0, 10).map((photo) => {
        return (
          <li key={photo.id} id={photo.id}>
            <img src={photo.url} alt={photo.title} />
          </li>
        );
      })}
    </ul>
  );
}
Photos.propTypes = {
  photosArray: PropTypes.array,
};

export default Photos;
