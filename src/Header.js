import React from "react";
import PropTypes from "prop-types";
function Header({ title = "Header title" }) {
  return <h3 style={{ fontSize: "20px" }}>{title}</h3>;
}
Header.propTypes = {
  title: PropTypes.string,
};
export default Header;
